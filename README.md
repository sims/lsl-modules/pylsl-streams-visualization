# LSL streams visualization

A basic QT interface:
* lists available LSL inlets 
* can individually enable the plots of each inlet (all channels of the selected inlet will be plotted)
* plot each channel of a given data inlet in a separate plot
* all Marker Inlets are plotted on one plot
* a button to update the inlet list
* a button to toggle selection of all inlets in the list

This code has been edited from the ReceiveAndPlot example available at https://github.com/labstreaminglayer/liblsl-Python/tree/master/pylsl/examples 

## Requirements

Python 3.x:
* numpy
* pylsl
* pyqt5==5.14
* pyqtgraph
