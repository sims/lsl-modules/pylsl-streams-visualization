#!/usr/bin/env python
""" Lists and plots LSL streams

A basic QT interface to list available inlets and select the ones to plot.
Will plot all channels of a given data inlet in a separate plot.
All Marker Inlets are plotted on one plot.
Interface includes a button to update the inlet list.

This code has been edited from:
ReceiveAndPlot example available at
https://github.com/labstreaminglayer/liblsl-Python/tree/master/pylsl/examples
"""
import sys
from typing import List
import math

import numpy as np
import pylsl
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui, QtWidgets


__author__ = "Marios Fanourakis"

# Basic parameters for the plotting window
plot_duration = 10  # how many seconds of data to show
update_interval = 30  # ms between screen updates
pull_interval = 50  # ms between each pull operation


class Inlet:
    """Base class to represent a plottable inlet"""
    def __init__(self, info: pylsl.StreamInfo):
        # create an inlet and connect it to the outlet we found earlier.
        # max_buflen is set so data older the plot_duration is discarded
        # automatically and we only pull data new enough to show it

        # Also, perform online clock synchronization so all streams are in the
        # same time domain as the local lsl_clock()
        # (see https://labstreaminglayer.readthedocs.io/projects/liblsl/ref/enums.html#_CPPv414proc_clocksync)
        # and dejitter timestamps
        self.inlet = pylsl.StreamInlet(info, max_buflen=plot_duration,
                                       processing_flags=pylsl.proc_clocksync | pylsl.proc_dejitter)
        # store the name and channel count
        self.name = info.name()
        self.channel_count = info.channel_count()
        self.uid = info.uid()
        self.plt_ixs: List[int] = []
        self.curves = None
        self.enabled = False

    def set_enabled(self, enabled: bool):
        self.enabled = enabled

    def pull_and_plot(self, plot_time: float, gl: pg.GraphicsLayout, plts: [pg.PlotItem]):
        """Pull data from the inlet and add it to the plot.
        :param plot_time: lowest timestamp that's still visible in the plot
        :param plts: the plot the data should be shown on
        """
        if not self.enabled:
            return
        else:
            # We don't know what to do with a generic inlet, so we skip it.
            pass


class DataInlet(Inlet):
    """A DataInlet represents an inlet with continuous, multi-channel data that
    should be plotted as multiple lines."""
    dtypes = [[], np.float32, np.float64, None, np.int32, np.int16, np.int8, np.int64]

    def __init__(self, info: pylsl.StreamInfo):
        super().__init__(info)
        # calculate the size for our buffer, i.e. two times the displayed data
        bufsize = (2 * math.ceil(info.nominal_srate() * plot_duration), info.channel_count())
        self.buffer = np.empty(bufsize, dtype=self.dtypes[info.channel_format()])


    def pull_and_plot(self, plot_time, gl: pg.GraphicsLayout, plts: [pg.PlotItem]):

        if not self.enabled:
            return
        if len(self.plt_ixs) == 0:
            empty = np.array([])
            # create one curve object for each channel/line that will handle displaying the data
            self.curves = [pg.PlotCurveItem(x=empty, y=empty, autoDownsample=True) for _ in range(self.channel_count)]
            ch_ix = 0
            for curve in self.curves:
                gl.nextRow()
                plts.append(gl.addPlot())
                plts[-1].addItem(curve)
                plts[-1].setXLink(plts[0])
                plts[-2].hideAxis('bottom')
                plts[-1].hideAxis('top')
                plts[-1].setTitle(title=self.name + ' channel ' + str(ch_ix))
                plts[-1].vb.setLimits(minYRange=20)
                self.plt_ixs.append(len(plts) - 1)
                ch_ix = ch_ix + 1

        # pull the data
        _, ts = self.inlet.pull_chunk(timeout=0.0,
                                      max_samples=self.buffer.shape[0],
                                      dest_obj=self.buffer)
        # ts will be empty if no samples were pulled, a list of timestamps otherwise
        if ts:
            ts = np.asarray(ts)
            y = self.buffer[0:ts.size, :]
            this_x = None
            old_offset = 0
            new_offset = 0
            for ch_ix in range(self.channel_count):
                # we don't pull an entire screen's worth of data, so we have to
                # trim the old data and append the new data to it
                old_x, old_y = self.curves[ch_ix].getData()
                # the timestamps are identical for all channels, so we need to do
                # this calculation only once
                if ch_ix == 0:
                    # find the index of the first sample that's still visible,
                    # i.e. newer than the left border of the plot
                    old_offset = old_x.searchsorted(plot_time)
                    # same for the new data, in case we pulled more data than
                    # can be shown at once
                    new_offset = ts.searchsorted(plot_time)
                    # append new timestamps to the trimmed old timestamps
                    this_x = np.hstack((old_x[old_offset:], ts[new_offset:]))
                # append new data to the trimmed old data
                this_y = np.hstack((old_y[old_offset:], y[new_offset:, ch_ix] - ch_ix))
                # replace the old data 
                # FIXME with temp fix for curves not showing if there are nan values
                self.curves[ch_ix].setData(this_x, np.nan_to_num(this_y), connect='finite')



class MarkerInlet(Inlet):
    """A MarkerInlet shows events that happen sporadically as vertical lines"""
    def __init__(self, info: pylsl.StreamInfo):
        super().__init__(info)

    def pull_and_plot(self, plot_time, gl: pg.GraphicsLayout, plts: [pg.PlotItem]):
        if not self.enabled:
            return
        # TODO: purge old markers
        strings, timestamps = self.inlet.pull_chunk(0)
        if timestamps:
            for string, ts in zip(strings, timestamps):
                plts[0].addItem(pg.InfiniteLine(ts, angle=90, movable=False, label=string[0]))


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        self.mainWidget = QtGui.QWidget()
        self.layout = QtGui.QGridLayout()
        self.mainWidget.setLayout(self.layout)

        self.plots = pg.GraphicsLayoutWidget()
        self.selectAllButton = QtGui.QPushButton('select/deselect all')
        self.checkTable = pg.CheckTable([''])
        self.updateButton = QtGui.QPushButton('Update inlets')

        self.layout.addWidget(self.selectAllButton, 0, 0)
        self.layout.addWidget(self.checkTable, 1, 0)
        self.layout.addWidget(self.updateButton, 2, 0)
        self.layout.addWidget(self.plots, 1, 1)

        self.setCentralWidget(self.mainWidget)


def main():
    app = QtWidgets.QApplication(sys.argv)
    win = MainWindow()
    win.show()

    # initialize with first plot (to display all marker data)
    plts: List[pg.PlotItem] = [win.plots.addPlot()]
    plts[-1].setTitle(title='All Inlets Marker Data')
    plts[-1].hideAxis('bottom')
    plts[-1].showAxis('top', show=True)

    inlets: List[Inlet] = []

    def remove_plots(inlet=None):
        if inlet is None:
            nonlocal inlets
            for inlet in inlets:
                for plt_ix in inlet.plt_ixs:
                    win.plots.removeItem(plts[plt_ix])
                inlet.plt_ixs = []
        else:
            for plt_ix in inlet.plt_ixs:
                win.plots.removeItem(plts[plt_ix])
            inlet.plt_ixs = []

    def update_inlets():
        # clear old inlet stuff
        nonlocal inlets
        for inlet in inlets:
            remove_plots(inlet)
        rows = win.checkTable.rowNames.copy()
        for row in rows:
            win.checkTable.removeRow(row)

        inlets = []

        # firstly resolve all streams that could be shown
        print("looking for streams")
        streams = pylsl.resolve_streams()

        # iterate over found streams, creating specialized inlet objects that will
        # handle plotting the data
        for info in streams:
            if info.type() == 'Markers':
                if info.nominal_srate() != pylsl.IRREGULAR_RATE \
                        or info.channel_format() != pylsl.cf_string:
                    print('Invalid marker stream ' + info.name())
                print('Adding marker inlet: ' + info.name())
                inlets.append(MarkerInlet(info))
                # give unique name to inlet and add to checkbox table
                inlets[-1].name = str(len(inlets) - 1) + ' ' + inlets[-1].name
                win.checkTable.addRow(inlets[-1].name)
            elif info.nominal_srate() != pylsl.IRREGULAR_RATE \
                    and info.channel_format() != pylsl.cf_string:
                print('Adding data inlet: ' + info.name())
                inlets.append(DataInlet(info))
                # give unique name to inlet and add to checkbox table
                inlets[-1].name = str(len(inlets) - 1) + ' ' + inlets[-1].name
                win.checkTable.addRow(inlets[-1].name)
            else:
                print('Don\'t know what to do with stream ' + info.name())
        return

    def scroll():
        """Move the view so the data appears to scroll"""
        # We show data only up to a timepoint shortly before the current time
        # so new data doesn't suddenly appear in the middle of the plot
        fudge_factor = pull_interval * .002
        plot_time = pylsl.local_clock()
        plts[0].setXRange(plot_time - plot_duration + fudge_factor, plot_time - fudge_factor)

    def update_data(inlet=None):
        # Read data from the inlet. Use a timeout of 0.0 so we don't block GUI interaction.
        mintime = pylsl.local_clock() - plot_duration
        # call pull_and_plot for each inlet.
        # Special handling of inlet types (markers, continuous data) is done in
        # the different inlet classes.
        if inlet is None:
            nonlocal inlets
            for inlet in inlets:
                inlet.pull_and_plot(mintime, win.plots, plts)
        else:
            inlet.pull_and_plot(mintime, win.plots, plts)

    def on_check_change(row, col, state):
        if type(state) is int:
            if state > 0:
                state = True
            else:
                state = False
        elif type(state) is not bool:
            print('State is of unknown type!! ')
            return

        print("check changed: " + row + " " + str(state))
        nonlocal inlets
        for inlet in inlets:
            if inlet.name == row:
                if state:
                    inlet.set_enabled(True)
                    update_data(inlet)
                else:
                    inlet.set_enabled(False)
                    remove_plots(inlet)

    def on_select_all_button_click():
        is_all_selected = True
        state = win.checkTable.saveState()

        # check if all are selected and if not, select
        for row in state['rows']:
            if not row[1]:
                is_all_selected = False
                row[1] = True

        # if all are selected, then deselect all
        if is_all_selected:
            for row in state['rows']:
                row[1] = False

        # update states on the checkmark table
        win.checkTable.restoreState(state)

    def on_update_button_click():
        # first update lsl inlets
        update_inlets()
        # then reapply the selections of the old inlets
        for row in win.checkTable.saveState()['rows']:
            on_check_change(row[0], None, row[1])

    # connect button events to relevant functions
    win.checkTable.sigStateChanged.connect(on_check_change)
    win.updateButton.clicked.connect(on_update_button_click)
    win.selectAllButton.clicked.connect(on_select_all_button_click)

    # initialize inlets and the inlet list
    update_inlets()

    # create a timer that will move the view every update_interval ms
    update_timer = QtCore.QTimer()
    update_timer.timeout.connect(scroll)
    update_timer.start(update_interval)

    # create a timer that will pull and add new data occasionally
    pull_timer = QtCore.QTimer()
    pull_timer.timeout.connect(update_data)
    pull_timer.start(pull_interval)

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
